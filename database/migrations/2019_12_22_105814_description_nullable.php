<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class DescriptionNullable
 * Esta migración podía no hacerla pero me di cuenta a medio programar la prueba de
 * que me olvidé de configurar cómo null el campo description y quise investigar cómo se actualizaban campos
 */
class DescriptionNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->mediumText('description')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->mediumText('description')->nullable(false)->change();
        });
    }
}
