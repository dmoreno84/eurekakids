<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class InsertSomeTestProducts
 * Migración que incluye algunos productos de prueba
 */
class InsertSomeTestProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('products')->insert(
            array(
                'name' => 'Cocinita retro green play kitchen',
                'description' => 'El peque podrá elaborar sus propios platos creativos e innovadores con esta cocina de juguete de madera realista. My Perfect Kitchen está equipada con unos fogones modernos con sonido de agua hirviendo y aceite salpicando y además tiene luces que simulan una llama de gas. Todo ello dará al pequeñín la sensación de estar cocinando en unos fogones reales. ¡Se convertirán en grandes chefs con esta cocina de juguete!
Edad: A partir de 3 años. ',
                'price' => 129.99,
                'stock' => 50
            )
        );
        DB::table('products')->insert(
            array(
                'name' => 'Pizarra 3 en 1 ajustable',
                'description' => '¡Esta pizarra es perfecta para los pequeños creativos! Con ella, los niños podrán expresarse a su manera y como quieran, ¡tienen diferentes opciones! Pueden usar pizarra blanca para escribir con rotulador o la pizarra negra para escribir con tiza. ¡Aunque también pueden usar piezas magnéticas para jugar!
Es ideal para estimular la creatividad, la imaginación y la concentración de los pequeños de la casa.
Edad: A partir de 3 años.',
                'price' => 49.99,
                'stock' => 40
            )
        );
        DB::table('products')->insert(
            array(
                'name' => 'Triciclo rito plegable gris',
                'description' => 'Este triciclo evolutivo de Rito es ideal para acompañar a tu peque en su camino hacia la autonomía. Los niños pedalearán mientras los papis los empujáis, fortaleciendo así sus músculos de forma progresiva y poco intrusiva. Gracias a su moderna estética, su resistente estructura y su comodidad de uso... ¡Será un placer pasear con los peques!

Este innovador Triciclo es muy cómodo de plegar y transportar además de ser cómodo de adaptar en las diferentes etapas del peque.
Edad: A partir de 10 meses. ',
                'price' => 119.99,
                'stock' => 65
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
