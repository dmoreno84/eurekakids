<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Product
 * Modelo que conecta con la tabla products de BDD
 * @package App
 */
class Product extends Model
{
    // Definimos los campos que se podrán rellenar del modelo
    protected $fillable = [
        'name',
        'description',
        'price',
        'stock'
    ];
}
