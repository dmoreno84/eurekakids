<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

/**
 * Class ProductController
 * Esta classe conntrola las acciones CRUD a realizar con los productos
 * @package App\Http\Controllers
 */
class ProductController extends Controller
{
    /**
     * Muestra una lista de los productos
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all(); // Obtenemos todos los productos

        return view('products.index', compact('products'));
    }

    /**
     * Muestra el formulario para crear un nuevo producto
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('products.create');
    }

    /**
     * Guarda el nuevo producto
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validamos los datos introducidos en el formulario, sino son correctos lanzará un error
        $request->validate([
            'product_name'=>'required',
            'product_price'=> 'required',
            'product_stock' => 'required|integer'
        ]);
        $product = new Product([
            'name' => $request->get('product_name'),
            'description' => $request->get('product_description'),
            'price' => $request->get('product_price'),
            'stock' => $request->get('product_stock')
        ]);
        $product->save();
        // Una vez creado el producto redirigimos a la vista principal
        return redirect('/products')->with('success', 'Producto creado correctamente!');
    }

    /**
     * Permite editar la información del producto seleccionado
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        return view('products.edit', compact('product'));
    }

    /**
     * Actualizará la información del producto seleccionado
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        // Validamos los datos introducidos en el formulario, sino son correctos lanzará un error
        $request->validate([
            'product_name'=>'required',
            'product_price'=> 'required',
            'product_stock' => 'required|integer'
        ]);
        $product->name = $request->get('product_name');
        $product->description = $request->get('product_description');
        $product->price = $request->get('product_price');
        $product->stock = $request->get('product_stock');
        $product->save();
        // Una vez editada la información del producto redirigimos a la vista principal
        return redirect('/products')->with('success', 'Producto "'.$product->name.'" actualizado correctamente!');
    }

    /**
     * Elimina el producto
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
        // Una vez eliminado el producto redirigimos a la vista principal
        return redirect('/products')->with('success', 'Producto "'.$product->name.'" eliminado correctamente!');
    }
}
