/* Archivo de configuraciones JS/Jquery de la web */
var formulariEliminar = null;

$( document ).ready(function() {
    $('#rangeProductPrice').on("input change", function(e) {
        $('#selectedPrice').val(e.currentTarget.value);
    });

    $('#selectedPrice').on('keyup', function(e){
        $('#rangeProductPrice').val(e.currentTarget.value);
    });

    $('.eliminarProducte').on('submit', function(e){
       formulariEliminar = this;
       e.preventDefault();
       $('#dialogEliminar').modal();
    });

    $('#dialogEliminar').on('show.bs.modal', function (e) {
        $('#eliminarProd').on('click', function(e){
            if(formulariEliminar != null){
                formulariEliminar.submit();
            }
        });
    });
    $('#dialogEliminar').on('hide.bs.modal', function (e) {
        $('#eliminarProd').off('click');
    });
});