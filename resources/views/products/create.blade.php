<!-- La vista extiende de la vista layout (principal) y rellena la sección 'content' -->
@extends('layout')

@section('content')
    <div class="container mt-3">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br />
        @endif
        <h2>Crear Nuevo Producto</h2>
        <form method="post" action="{{ route('products.store') }}">
            @csrf
            <div class="form-group">
                <label for="inputProductName">Nombre Producto:</label>
                <input type="text" class="form-control" id="inputProductName" name="product_name"/>
            </div>
            <div class="form-group">
                <label for="textProductDesc">Descripción Producto.</label>
                <textarea class="form-control rounded-0" id="textProductDesc" name="product_description" rows="3"></textarea>
            </div>
            <div class="form-group">
                <label for="rangeProductPrice">Precio Producto:</label>
                <input type="range" class="form-control-range" id="rangeProductPrice" value="0" min="0" step="0.01" max="1000">
                <input type="text" class="mt-2" name="product_price" style="width: 75px;" id="selectedPrice" value="0" /> €
            </div>
            <div class="form-group">
                <label for="inputProductStock">Stock:</label>
                <input type="number" class="form-control input-stock" id="inputProductStock" name="product_stock"/>
            </div>
            <button type="submit" class="btn btn-primary">Crear</button>
        </form>
    </div>
@endsection