<!-- La vista extiende de la vista layout (principal) y rellena la sección 'content' -->
@extends('layout')

@section('content')
    <div class="container mt-3">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br />
        @endif
        <h2>Editar {{ $product->name }}</h2>
        <form method="post" action="{{ route('products.update', $product) }}">
            @method('PATCH')
            @csrf
            <div class="form-group">
                <label for="inputProductName">Nombre Producto:</label>
                <input type="text" class="form-control" id="inputProductName" name="product_name" value="{{ $product->name }}" />
            </div>
            <div class="form-group">
                <label for="textProductDesc">Descripción Producto.</label>
                <textarea class="form-control rounded-0" id="textProductDesc" name="product_description" rows="3">{{ $product->description }}</textarea>
            </div>
            <div class="form-group">
                <label for="rangeProductPrice">Precio Producto:</label>
                <input type="range" class="form-control-range" id="rangeProductPrice" value="{{ $product->price }}" min="0" step="0.01" max="1000">
                <input type="text" class="mt-2" name="product_price" style="width: 75px;" id="selectedPrice" value="{{ $product->price }}" /> €
            </div>
            <div class="form-group">
                <label for="inputProductStock">Stock:</label>
                <input type="number" class="form-control input-stock" id="inputProductStock" name="product_stock" value="{{ $product->stock }}" />
            </div>
            <button type="submit" class="btn btn-primary">Editar</button>
        </form>
    </div>
@endsection