<!-- La vista extiende de la vista layout (principal) y rellena la sección 'content' -->
@extends('layout')

@section('content')
    <div class="container mt-3">
        @if(session()->get('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div><br />
        @endif
        <table class="table table-striped">
            <thead>
            <tr>
                <td>Nombre Producto</td>
                <td>Precio Producto</td>
                <td>Stock</td>
                <td>&nbsp;</td>
            </tr>
            </thead>
            <tbody>
            @foreach($products as $prod)
                <tr>
                    <td>{{$prod->name}}</td>
                    <td>{{$prod->price}}</td>
                    <td>{{$prod->stock}}</td>
                    <td>
                        <a href="{{ route('products.edit',$prod)}}" class="btn btn-primary float-left">Editar</a>
                        <form class="eliminarProducte" action="{{ route('products.destroy', $prod)}}" method="post">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger float-left ml-lg-1 mt-lg-0 mt-md-1 mt-sm-1 mt-xs-1" type="submit">Eliminar</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection