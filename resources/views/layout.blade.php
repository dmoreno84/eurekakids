<!DOCTYPE html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('parts.head')
@include('parts.header')

<body>
<!-- Contenido de la página -->
<div class="container">
    <div class="row">
        @yield('content')
    </div>
</div>

@include('parts.footer')

</body>

</html>